﻿
namespace ONELIMS
{
    partial class TO_Tests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtb_remarks = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtp_performed = new System.Windows.Forms.DateTimePicker();
            this.btn_save = new System.Windows.Forms.Button();
            this.tb_CaseNo = new System.Windows.Forms.TextBox();
            this.tb_PatientNum = new System.Windows.Forms.TextBox();
            this.cb_roomNo = new System.Windows.Forms.ComboBox();
            this.cb_sex = new System.Windows.Forms.ComboBox();
            this.tb_HMO = new System.Windows.Forms.TextBox();
            this.tb_chargeSlipNum = new System.Windows.Forms.TextBox();
            this.tb_ExamNo = new System.Windows.Forms.TextBox();
            this.tb_age = new System.Windows.Forms.TextBox();
            this.tb_reqPhysician = new System.Windows.Forms.TextBox();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_testAdded = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.lb_TONumber = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testAdded)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lb_TONumber);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.dgv_testAdded);
            this.groupBox1.Controls.Add(this.rtb_remarks);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tb_CaseNo);
            this.groupBox1.Controls.Add(this.tb_PatientNum);
            this.groupBox1.Controls.Add(this.cb_roomNo);
            this.groupBox1.Controls.Add(this.cb_sex);
            this.groupBox1.Controls.Add(this.tb_HMO);
            this.groupBox1.Controls.Add(this.tb_chargeSlipNum);
            this.groupBox1.Controls.Add(this.tb_ExamNo);
            this.groupBox1.Controls.Add(this.tb_age);
            this.groupBox1.Controls.Add(this.tb_reqPhysician);
            this.groupBox1.Controls.Add(this.tb_Name);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtp_performed);
            this.groupBox1.Location = new System.Drawing.Point(12, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1132, 622);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // rtb_remarks
            // 
            this.rtb_remarks.Location = new System.Drawing.Point(196, 549);
            this.rtb_remarks.Name = "rtb_remarks";
            this.rtb_remarks.Size = new System.Drawing.Size(374, 53);
            this.rtb_remarks.TabIndex = 25;
            this.rtb_remarks.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(55, 566);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 29);
            this.label15.TabIndex = 24;
            this.label15.Text = "Remarks";
            // 
            // dtp_performed
            // 
            this.dtp_performed.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_performed.Location = new System.Drawing.Point(178, 223);
            this.dtp_performed.Name = "dtp_performed";
            this.dtp_performed.Size = new System.Drawing.Size(393, 34);
            this.dtp_performed.TabIndex = 23;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(968, 37);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(158, 44);
            this.btn_save.TabIndex = 22;
            this.btn_save.Text = "SAVE";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // tb_CaseNo
            // 
            this.tb_CaseNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_CaseNo.Location = new System.Drawing.Point(209, 495);
            this.tb_CaseNo.Name = "tb_CaseNo";
            this.tb_CaseNo.Size = new System.Drawing.Size(361, 34);
            this.tb_CaseNo.TabIndex = 20;
            // 
            // tb_PatientNum
            // 
            this.tb_PatientNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_PatientNum.Location = new System.Drawing.Point(209, 438);
            this.tb_PatientNum.Name = "tb_PatientNum";
            this.tb_PatientNum.Size = new System.Drawing.Size(361, 34);
            this.tb_PatientNum.TabIndex = 19;
            // 
            // cb_roomNo
            // 
            this.cb_roomNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_roomNo.FormattingEnabled = true;
            this.cb_roomNo.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cb_roomNo.Location = new System.Drawing.Point(473, 164);
            this.cb_roomNo.Name = "cb_roomNo";
            this.cb_roomNo.Size = new System.Drawing.Size(98, 37);
            this.cb_roomNo.TabIndex = 18;
            // 
            // cb_sex
            // 
            this.cb_sex.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_sex.FormattingEnabled = true;
            this.cb_sex.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cb_sex.Location = new System.Drawing.Point(246, 164);
            this.cb_sex.Name = "cb_sex";
            this.cb_sex.Size = new System.Drawing.Size(98, 37);
            this.cb_sex.TabIndex = 1;
            // 
            // tb_HMO
            // 
            this.tb_HMO.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_HMO.Location = new System.Drawing.Point(209, 392);
            this.tb_HMO.Name = "tb_HMO";
            this.tb_HMO.Size = new System.Drawing.Size(361, 34);
            this.tb_HMO.TabIndex = 17;
            // 
            // tb_chargeSlipNum
            // 
            this.tb_chargeSlipNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_chargeSlipNum.Location = new System.Drawing.Point(209, 333);
            this.tb_chargeSlipNum.Name = "tb_chargeSlipNum";
            this.tb_chargeSlipNum.Size = new System.Drawing.Size(361, 34);
            this.tb_chargeSlipNum.TabIndex = 16;
            // 
            // tb_ExamNo
            // 
            this.tb_ExamNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ExamNo.Location = new System.Drawing.Point(209, 276);
            this.tb_ExamNo.Name = "tb_ExamNo";
            this.tb_ExamNo.Size = new System.Drawing.Size(361, 34);
            this.tb_ExamNo.TabIndex = 15;
            // 
            // tb_age
            // 
            this.tb_age.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_age.Location = new System.Drawing.Point(72, 167);
            this.tb_age.Name = "tb_age";
            this.tb_age.Size = new System.Drawing.Size(118, 34);
            this.tb_age.TabIndex = 2;
            // 
            // tb_reqPhysician
            // 
            this.tb_reqPhysician.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_reqPhysician.Location = new System.Drawing.Point(201, 111);
            this.tb_reqPhysician.Name = "tb_reqPhysician";
            this.tb_reqPhysician.Size = new System.Drawing.Size(370, 34);
            this.tb_reqPhysician.TabIndex = 14;
            // 
            // tb_Name
            // 
            this.tb_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Name.Location = new System.Drawing.Point(106, 54);
            this.tb_Name.Name = "tb_Name";
            this.tb_Name.Size = new System.Drawing.Size(465, 34);
            this.tb_Name.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 228);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(127, 29);
            this.label14.TabIndex = 13;
            this.label14.Text = "Performed";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(86, 498);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 29);
            this.label13.TabIndex = 12;
            this.label13.Text = "Case No";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(68, 441);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 29);
            this.label12.TabIndex = 11;
            this.label12.Text = "Patient No";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(68, 392);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 29);
            this.label11.TabIndex = 10;
            this.label11.Text = "HMO/Coop";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(216, 392);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 29);
            this.label10.TabIndex = 9;
            this.label10.Text = " ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(350, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 29);
            this.label9.TabIndex = 8;
            this.label9.Text = "Room No.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(196, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 29);
            this.label8.TabIndex = 7;
            this.label8.Text = "Sex";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 29);
            this.label7.TabIndex = 6;
            this.label7.Text = "Age";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(213, 333);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = " ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(31, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "ChargeSlip No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Req. Physician";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(213, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = " ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Examination No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // dgv_testAdded
            // 
            this.dgv_testAdded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_testAdded.Location = new System.Drawing.Point(588, 111);
            this.dgv_testAdded.Name = "dgv_testAdded";
            this.dgv_testAdded.RowHeadersWidth = 51;
            this.dgv_testAdded.RowTemplate.Height = 24;
            this.dgv_testAdded.Size = new System.Drawing.Size(526, 491);
            this.dgv_testAdded.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 36);
            this.label16.TabIndex = 27;
            this.label16.Text = "TO #";
            // 
            // lb_TONumber
            // 
            this.lb_TONumber.AutoSize = true;
            this.lb_TONumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TONumber.Location = new System.Drawing.Point(101, 18);
            this.lb_TONumber.Name = "lb_TONumber";
            this.lb_TONumber.Size = new System.Drawing.Size(67, 29);
            this.lb_TONumber.TabIndex = 28;
            this.lb_TONumber.Text = "TO #";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(588, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 33);
            this.button1.TabIndex = 23;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TO_Tests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 757);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_save);
            this.Name = "TO_Tests";
            this.Text = "TO_Tests";
            this.Load += new System.EventHandler(this.TO_Tests_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testAdded)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtb_remarks;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtp_performed;
        private System.Windows.Forms.TextBox tb_CaseNo;
        private System.Windows.Forms.TextBox tb_PatientNum;
        private System.Windows.Forms.ComboBox cb_roomNo;
        private System.Windows.Forms.ComboBox cb_sex;
        private System.Windows.Forms.TextBox tb_HMO;
        private System.Windows.Forms.TextBox tb_chargeSlipNum;
        private System.Windows.Forms.TextBox tb_ExamNo;
        private System.Windows.Forms.TextBox tb_age;
        private System.Windows.Forms.TextBox tb_reqPhysician;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lb_TONumber;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dgv_testAdded;
    }
}