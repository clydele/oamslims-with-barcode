﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ONELIMS
{
    public partial class Available_Test_Items : Form
    {
        public MySqlConnection conn;
        public MySqlCommand cmd = new MySqlCommand();
        public MySqlDataAdapter DA = new MySqlDataAdapter();
        public MySqlDataReader reader = null;
        public DataSet DS = new DataSet();
        public DataTable DT = new DataTable();
        private string testOrder = "";
        public Available_Test_Items(string curTO)
        {
            InitializeComponent();
            SetConnection();
            showAvailableTests();
            testOrder = curTO;
        }

        private void Available_Test_Items_Load(object sender, EventArgs e)
        {

        }
        public void SetConnection()
        {
            DBConnect databaseConnector = new DBConnect();
            conn = databaseConnector.conn;
        }

        public void showAvailableTests()
        {
            dgv_availableTest.Columns.Clear();
            string retrieveTOSql = "Select* from tbl_testitems";
            conn.Open();
            DA = new MySqlDataAdapter(retrieveTOSql, conn);
            DS.Reset();
            DA.Fill(DS);
            DT = DS.Tables[0];
            dgv_availableTest.DataSource = DT;
            conn.Close();

            DataGridViewCheckBoxColumn dcb_selecteItem = new DataGridViewCheckBoxColumn();
            dcb_selecteItem.HeaderText = "Select Test"; 
            dcb_selecteItem.Name = "dcb_selectedItem";
            dcb_selecteItem.TrueValue = "Selected";
            dcb_selecteItem.FalseValue = "Not Selected";
            int dgvToColCount = dgv_availableTest.ColumnCount;
            dgv_availableTest.Columns.Insert(dgvToColCount, dcb_selecteItem);





        }
    }
}
