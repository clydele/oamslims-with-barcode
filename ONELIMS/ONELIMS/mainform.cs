﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace ONELIMS
{
  
    public partial class mainform : Form
    {
        public MySqlConnection conn;
        public MySqlCommand cmd =new MySqlCommand();
        public MySqlDataAdapter DA = new MySqlDataAdapter();
        public MySqlDataReader reader = null;
   
        public DataTable DT = new DataTable();

        public mainform()
        {
            InitializeComponent();
            setConnection();
            RefreshDataGridViews();
        }
        public void setConnection()
        {
            DBConnect databaseConnector = new DBConnect();
            conn = databaseConnector.conn;
        }
        public void ExecuteQuery(string SQLCommand,string successfulMessage,string ErrorMessage)
        {
            conn.Open();
            cmd = conn.CreateCommand();
            cmd.CommandText = SQLCommand;
             cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            try
            {
                int a=cmd.ExecuteNonQuery();
                MessageBox.Show(successfulMessage);
            }
            catch(Exception err)
            {
                MessageBox.Show("Error while executing the code\n:" + err.ToString());
            }
          
            conn.Close();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
        public void RefreshDataGridViews()
        {
            showTestOrders();
            showTestOrdersForCollection();
        }
        public void showTestOrders()
        {
            dgv_TestRegistrations.Columns.Clear();
            string retrieveTOSql = "Select* from tbl_testorder WHERE STATUS='Created'";
            conn.Open();
            DA = new MySqlDataAdapter(retrieveTOSql, conn);
            DataSet DS = new DataSet();
            DS.Reset();
            DA.Fill(DS);
            DT = DS.Tables[0];
            dgv_TestRegistrations.DataSource = DT;
            conn.Close();

            DataGridViewCheckBoxColumn cbox_selectTO = new DataGridViewCheckBoxColumn();
            cbox_selectTO.Name = "cbox_selectTO";
            cbox_selectTO.HeaderText = "Select TO";
            cbox_selectTO.TrueValue = "Selected";
            cbox_selectTO.FalseValue = "Not Selected";
            int a = dgv_TestRegistrations.ColumnCount;
            dgv_TestRegistrations.Columns.Insert(a,cbox_selectTO);
           

        }
        public void showTestOrdersForCollection()
        {
            dgv_TOsForCollection.Columns.Clear();
            string retrieveTOSql = "Select* from tbl_testorder WHERE  Status='For Collection'";
            conn.Open();
            DA = new MySqlDataAdapter(retrieveTOSql, conn);
            DataSet DS = new DataSet();
            DS.Reset();
            DA.Fill(DS);
            DT = DS.Tables[0];
            dgv_TOsForCollection.DataSource = DT;
            conn.Close();

            DataGridViewButtonColumn dgvbtn_markAsCollected = new DataGridViewButtonColumn();
            dgvbtn_markAsCollected.Name = "dgv_markAsCollected";
            dgvbtn_markAsCollected.HeaderText = "Collected";
            dgvbtn_markAsCollected.Text = "Collected";
            dgvbtn_markAsCollected.UseColumnTextForButtonValue = true;

            int a = dgv_TOsForCollection.ColumnCount;
            dgv_TOsForCollection.Columns.Insert(a, dgvbtn_markAsCollected);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form TestOrderControl = new Test_Order();
            TestOrderControl.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string> TOsForCollection = new List<string>();



            for (int TORows = 0; TORows < dgv_TestRegistrations.Rows.Count - 1; TORows++)
            {
                DataGridViewCell CB_TOSelect = dgv_TestRegistrations.Rows[TORows].Cells[0];
                
                if(CB_TOSelect.Value!=null)
                if (String.Compare(dgv_TestRegistrations.Rows[TORows].Cells[0].Value.ToString(),"Selected")==0)
                {
                    TOsForCollection.Add(dgv_TestRegistrations.Rows[TORows].Cells[1].Value.ToString());
                }
               
            }
            
            conn.Open();
            cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            String[] SelectedTOsForCollection = TOsForCollection.ToArray();
            for (int b = 0; b < SelectedTOsForCollection.Length; b++)
            {
                try
                {
                    cmd.CommandText = "UPDATE tbl_testorder SET Status='For Collection' WHERE TONumber=\"" + SelectedTOsForCollection[b] + "\"";
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    
                   
                  
                }
                catch(Exception err)
                {
                    throw;
                }
                
            }
            conn.Close();
            RefreshDataGridViews();
        
            }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            showTestOrders();
        }

        private void dgv_TOsForCollection_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colname = dgv_TOsForCollection.Columns[e.ColumnIndex].Name.ToString();
            string TestOrderID = dgv_TOsForCollection.CurrentRow.Cells[1].Value.ToString();
            string PatientName = dgv_TOsForCollection.CurrentRow.Cells[2].Value.ToString();
           

            if (String.Compare(colname,"dgv_markAsCollected")==0)
            {
                
                string SendToSectionsSQL ="UPDATE tbl_testorder SET Status='Collected' WHERE TONumber=\""+TestOrderID+"\"";
         
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = SendToSectionsSQL;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;

                try
                {
                    int a = cmd.ExecuteNonQuery();
                    Form barcodeForm = new Sample_Barcode(TestOrderID,TestOrderID, PatientName,TestOrderID);
                    barcodeForm.Show();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error while executing the code\n:" + err.ToString());
                }

                conn.Close();
            }
            
        }
    }
}
