﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ONELIMS
{
    public partial class TO_Tests : Form
    {
        public TO_Tests(string TONumber)
        {
            InitializeComponent();
            lb_TONumber.Text = TONumber;
        }

        private void TO_Tests_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form addTests = new Available_Test_Items(lb_TONumber.Text);
            addTests.ShowDialog();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
