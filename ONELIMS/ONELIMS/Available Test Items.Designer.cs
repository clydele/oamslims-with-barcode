﻿
namespace ONELIMS
{
    partial class Available_Test_Items
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_availableTest = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_finish = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_availableTest)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_availableTest
            // 
            this.dgv_availableTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_availableTest.Location = new System.Drawing.Point(12, 59);
            this.dgv_availableTest.Name = "dgv_availableTest";
            this.dgv_availableTest.RowHeadersWidth = 51;
            this.dgv_availableTest.RowTemplate.Height = 24;
            this.dgv_availableTest.Size = new System.Drawing.Size(461, 391);
            this.dgv_availableTest.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(536, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(8, 8);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btn_finish
            // 
            this.btn_finish.Location = new System.Drawing.Point(373, 12);
            this.btn_finish.Name = "btn_finish";
            this.btn_finish.Size = new System.Drawing.Size(99, 41);
            this.btn_finish.TabIndex = 2;
            this.btn_finish.Text = "Finish";
            this.btn_finish.UseVisualStyleBackColor = true;
            // 
            // Available_Test_Items
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 472);
            this.Controls.Add(this.btn_finish);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgv_availableTest);
            this.Name = "Available_Test_Items";
            this.Text = "Available_Test_Items";
            this.Load += new System.EventHandler(this.Available_Test_Items_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_availableTest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_availableTest;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_finish;
    }
}