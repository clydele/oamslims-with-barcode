﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ONELIMS
{
    public partial class Test_Order : Form
    {
        public MySqlConnection conn;
        public MySqlCommand cmd = new MySqlCommand();
        public MySqlDataAdapter DA = new MySqlDataAdapter();
        public MySqlDataReader reader = null;
        public DataTable DT = new DataTable();
        public Test_Order()
        {
            InitializeComponent();        
            SetConnection();
            showTestOrders();
        }
        public void SetConnection()
        {
            DBConnect databaseConnector = new DBConnect();
            conn = databaseConnector.conn;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Test_Order_Load(object sender, EventArgs e)
        {
                 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string saveTO = "INSERT INTO tbl_testorder VALUES(null,\""+tb_Name.Text+"\",\""+tb_ExamNo.Text+"\",\""+tb_PatientNum.Text+ "\",\""+tb_reqPhysician.Text+ "\",\""+tb_chargeSlipNum.Text+ "\",\""+tb_CaseNo.Text+ "\",\""+tb_age.Text+ "\",\""+cb_sex.Text+ "\",\""+cb_roomNo.Text+ "\",\""+tb_HMO.Text+ "\",\""+dtp_performed.Value+"\",'Created',\""+rtb_remarks.Text+"\")";
            ExecuteQuery(saveTO, "Test Order Saved");
        }
        public void showTestOrders()
        {
            dgv_TOs.Columns.Clear();
            string retrieveTOSql = "Select* from tbl_testorder";
            conn.Open();
            DA = new MySqlDataAdapter(retrieveTOSql, conn);
            DataSet DS = new DataSet();
            DS.Reset();
            DA.Fill(DS);
            DT = DS.Tables[0];
            dgv_TOs.DataSource = DT;
            conn.Close();

            DataGridViewCheckBoxColumn cbox_selectTO = new DataGridViewCheckBoxColumn();
            cbox_selectTO.Name = "cbox_selectTO";
            cbox_selectTO.HeaderText = "Select TO";
            cbox_selectTO.TrueValue = "Selected";
            cbox_selectTO.FalseValue = "Not Selected";
            int a = dgv_TOs.ColumnCount;
            dgv_TOs.Columns.Insert(a, cbox_selectTO);



        }
            public void ExecuteQuery(string SQLCommand,string successMessage)
        {
            conn.Open();
            cmd = conn.CreateCommand();
            cmd.CommandText = SQLCommand;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show(successMessage);
            }
            catch (Exception err)
            {
                MessageBox.Show("Error while executing the request \n:" + err.ToString());
            }
            cmd.Dispose();
            conn.Close();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void dgv_TOs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string TONumber = dgv_TOs.CurrentRow.Cells[1].Value.ToString();
            Form testordertests = new TO_Tests(TONumber);
            testordertests.ShowDialog();
        }

   
    }
}
