﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IronBarCode;
namespace ONELIMS
{
    public partial class Sample_Barcode : Form
    {
        private string BarcodeText = "";
        private string PatientName = "";
        private string PatientSampleID = "";
        private string PatientTOID = "";
        public Sample_Barcode(string SampleThrown,string Name,string SampleID,string TestOrderID)
        {
            InitializeComponent();
            BarcodeText = SampleThrown;
            PatientName = Name;
            PatientSampleID =SampleID;
            PatientTOID = TestOrderID;

            tb_Name.Text = Name;
            tb_sampleID.Text = SampleID;
            tb_TOID.Text = TestOrderID;
      
        }

        private void Sample_Barcode_Load(object sender, EventArgs e)
        {

        }
        public void CreateBarcode(string SampleID)
        {

            GeneratedBarcode SampleBarcode = IronBarCode.BarcodeWriter.CreateBarcode(SampleID, BarcodeWriterEncoding.Code128);
            SampleBarcode.SaveAsPng("barcodes/Barcode" + SampleID+".png");
            System.Diagnostics.Process.Start(Application.StartupPath.ToString() + "/barcodes/Barcode" + SampleID + ".png");
          


        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateBarcode(BarcodeText);
        }
    }

}
